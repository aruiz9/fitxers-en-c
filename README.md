# Fonaments de gestió de fitxers #
## Index ##
- Alta   
- Baixa
- Consulta
- Modificació
- Consulta esborrats
- Esborrar fitxer
- Compactar el fitxer
- Acces directe
- Numero de registres
- Generar informe

### Alta ###
L'opció alta és tracta d’obrir el fitxer, fer el tractament de les
dades en aquest cas salvar els registres, i finalment tancar el fitxer.
Una bona opció per fer l’alta és obrir el fitxer en mode “ab”, afegir en binari. Aquest
mode deixa l’apuntador després del darrer registre preparat per afegir dades al fitxer.
A més, si el fitxer no existeix el crea.

Cal pensar sempre en la robustesa. Fixar-se en la funció d'obrir fitxer retorna valor
NULL si, per algun motiu, no s’ha pogut obrir correctament el fitxer.

Recordar també que la funció fwrite retorna el nombre de registres que
s’han pogut escriure correctament. Sempre te que coincidir amb el tercer
paràmetre. Altrament hi ha hagut un error.

### Baixa ###
Per fer la baixa hem d’obrir el fitxer en mode “rb+” Lectura/Escriptura en binari. Si algú
pensa en l’opció “wb+” penseu que els modes “w” sobreescriuen el fitxer esborrant
totes les dades. 
>Com que el sistema operatiu no ens facilita cap opció per fer baixa de registres. Vam tindre que gestionar nosltres mateixos l'opció baixa de registres.

Vam tindre que crear un nou camp dintre de l'estructura abans creada, per al correcte funcionament de la nova funcio que crearem per a eliminar registres dintre del fitcher.
L'estràtegia consistia en assignar un * als registres que l'usuario volia esborrar. Aixi des de aquell moment que l'usuari eliminaba el registre totes les operacions de gestió consideraran que els registres amb un asterisc estan esborrats.

`if((n=fread(&p,sizeof(Persona),1,f))!=1){`
 `if(!feof(f)) {`
`if(n!=0)` { printf("\nBaixa: Error de lectura"); return -1; }`
 `if( p.marcaBorrat!='*' && strcmp(p.nif,nifTmp)==0 ){`

Quan vam utilitzar la funcio fread ens vam topar amb un petit problema:
l’apuntador del fitxer passa al següent registre. Això significa que quan hem trobat el registre que volem esborrar, l’apuntador està sobre el
següent registre. 
Per a solucionar aquet petit problema podiem solucionar-ho de dues maneres:
- Una primera opció seria crear un fitxer temporal i escriure-hi tots els registres
menys el que volem donar de baixa. Després esborrar el fitxer inicial i canviar el
nom del nou fitxer pel nom del fitxer inicial.
- L’altra opció, la que nosaltres utilitzarem, consisteix en endarrerir l’apuntador
un registre. Així el situaríem sobre del registre que volem esborrar. (es la que vam utilitzar dintre del nostre programa)

Pero per a que funcionesi vam tindre que utilitzar una nova funcio:

`int fseek(FILE *f, long desplaçament, int posicio);`

On:
- FILE *f és una referència al fitxer sobre el qual volem fer moure l’apuntador
- long desplaçament és el nombre de bytes que volem desplaçar l’apuntador
- int posicio és el punt a partir del qual farem el desplaçament. 

En definitiva per fer la baixa fem un accés seqüencial al fitxer cercant el registre i després
desplacem l’apuntador del fitxer uns bytes endarrere amb la funció fseek,
concretament ens desplacem un registre, i finalment ens quedaria tancar el fitxer.

### Consulta ###
Aquesta opció es tracta d’obrir el fitxer en mode “rb”, recórrer el fitxer fent successives lectures i mostrar els registres que no estan marcats per
esborrar. Finalment tancaríem el fitxer. 

### Modificacio ###
Es tracta de la mateixa estratègia que la baixa. L’únic canvi era, un cop trobat el
registre, assignar totes les noves dades i no només l’asterisc. També ens caldrà moure
l’apuntador un registre endarrere.

### Consulta esborrats ###
Es el mateix codi que l’opció consulta però en comptes de mostrar els que no tenen un asterisc al camp
marcaBorrat, caldria mostrar els que sí que tenen un asterisc.

### Esborrar fitxer ###
Aquesta opció ens permetrà esborrar el fitxer de dades que tenim al dispositiu extern.
Per fer-ho ens cal una nova funció:

`int unlink(nomFitxer);`

On:
- nomFitxer és el nom del fitxer extern. En el nostre cas “persones.dat”.

### Compactar el fitxer ###

Aquesta opció ens permetrà esborrar els registres marcats per esborrar. Això significa
que els registres marcats per esborrar ja no estaran físicament al fitxer extern.

- Obrir el fitxer que volem compactar,en mode (“rb”).
- Obrir un fitxer temporal on escriurem els registres que no estan marcats per esborrar,en mode (“wb”). 
- Llegir tots els registre, seqüencialment, del fitxer i escriure’ls al fitxer nou sempre i quan no estiguin marcats per esborrar.
- Un cop copiats els registres, tancar els dos fitxers.
- Esborrar el fitxer antic.
- I finalment renombrar el fitxer nou amb el nom del fitxer antic.
Per renombrar vam utlitzar una nova funcio:
`int rename( fitxerVell , fitxerNou )`
La funció retorna -1 si no ha pogut renombrar el fitxer. Altrament retorna un
valor diferent.

### Acces directe ###
Els registres tenien longitud fixa. Podríem, doncs, cridar a la funció fseek per accedir directament
per posició a un determinat registre de l’arxiu.
Per fer el codi robust caldria comprovar abans si el registre al qual volem accedir existeix.
Podríem fer una versió per saltar a qualsevol posició, endavant o endarrere.

`n= pos-1; //El primer registre és troba al byte 0`
`fseek( f , (long) (n*sizeof(Animal)) , SEEK_SET);`
- pos: número de registre al qual volem accedir.
- (n*sizeof(Animal)): posició física (en bytes) del registre al qual volem accedir.
- f apuntador al fitxer.
- SEEK_SET el salt es fa amb referència a l’inici del fitxer.

### Numero de registres ###
Per saber el nombre de registres que hi ha en un fitxer podem fer:
long nReg;

`fseek( f , (long) 0L , SEEK_END); //situar-se al darrer registre``
`nReg= ftell (f); //calcular el que ocupen els n-1 registres`
`nReg= nReg/sizeof(Animal); //calcular el número de registres`
`//A més hauríem de restar els registres marcats per esborrar`

### Informe ###
Per generar l'informe vam tindre que obrir el fitxer de text en mode `w` i el fitxer animals en mode `r`, despres vam escriure la capçalera estàtica del fitxer html, acte seguit recorriam el fitxer animal, llegiam l'animal i si no estaba marcat per esborrats, convertiam a text el registre binari i creavem la fila de la taula en format html.
Escribiam al fitxer informe el peu estàtica html, a continuacio tancavem els dos fitxers. 


