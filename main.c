#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define MAX_NOM 15
#define MAX_TIPUS 35
#define NOM_FITXER "animals.dat"
#define BB while(getchar()!='\n')
#define ERROR_OBRIR_FITXER "Error en obrir el fitxer"
#define ERROR_ESCRIPTURA_FITXER "Error d'escriptura al fitxer"
#define ERROR_LECTURA_FITXER "Error de lectura al fitxer"

typedef struct{
    char tipus[MAX_TIPUS+1];
    char nom[MAX_NOM+1];
    char sexe;// m-masculi, f-femeni
    bool enPerillExtincio;
    int edat;
    char marcaBorrat;
    double pes;
} Animal; //estructura que representa el fitxer intern.
void generarInforme();
void escriureAnimal(Animal perso);
void entrarAnimal(Animal *animal);
void entraOpcio(int *opcio);
void pintarMenu();
int alta(char nomFitxer[]);
int consulta(char nomFitxer[],bool);
int baixa(char nomfitxer[]);
int numeroDeRegistres(nomFitxer[]);
void consultaresborrats(nomFitxer[]);
void esborrarFitcher(nomFitcher[]);
void comprimirFitcher(nomFitcher[]);
int main()

{
    bool esborrats=false;
    int opcio, error;
    int acces=0;
    char car;
    do{
        pintarMenu();
        entraOpcio(&opcio);
        switch(opcio){
        case 1:
            error=alta(NOM_FITXER);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-2) printf(ERROR_ESCRIPTURA_FITXER);
            break;
        case 2:
            do{
            error=baixa(NOM_FITXER,esborrats);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-2) printf(ERROR_ESCRIPTURA_FITXER);
            if(error==-3) printf(ERROR_LECTURA_FITXER);
            }while ()
            break;

        case 3:
            error=consulta(NOM_FITXER,esborrats);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-2) printf(ERROR_ESCRIPTURA_FITXER);
            if(error==-3) printf(ERROR_LECTURA_FITXER);
            break;
        case 4:
        do{
            error=baixa(NOM_FITXER,esborrats);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-2) printf(ERROR_ESCRIPTURA_FITXER);
            if(error==-3) printf(ERROR_LECTURA_FITXER);
          }while()
        case 5:
            do{
                printf("Mirar esborrats");
                scanf("%c",car);
                if (car=='S'){
                    consultaresborrats();
                }
            while (car!='N')
            break;
        case 6:
            printf("Estas segur de que vols esborrar el fitcher:(S/N): ")
            scanf("%c",car);
            if(car=='S'){
                esborrarFitcher();
            }else continuar();
        case 7:
            printf("Estas segur de que vols comprimir el fitcher:(S/N): ")
            scanf("%c",car);
            if(car=='S'){
                comprimirFitcher();
            }else continuar();
        case 8:
            nReg=numeroDeRegistres(nomFitxer[]);
            printf("%d",nReg);
            break;
        case 9:
        do{
        accesDirecte(nomFitxer[],&acces);
        }while(acces!=0)
        break;
        case 10:
        generarInforme();
        break;

    return 0;
}



void entraOpcio(int *opcio){
    scanf("%d",opcio);BB;
}

void pintarMenu(){
    system("clear");
    printf("**** MENU *****\n");
    printf("***************\n\n");
    printf("\t1 - Alta\n");
    printf("\t2 - Baixa\n");
    printf("\t3 - Consulta\n");
    printf("\t4 - Modificar\n");
    printf("\t5 - Consulta Esborrats\n");
    printf("\t6 - Esborrar fitxer\n");
    printf("\t7 - Compactar fitxer\n\n");
    printf("\t0 - Sortir\n\n");
    printf("Tria opci�: (0-7)");
}

int alta(char nomFitxer[]){
    Animal a1;
    FILE *f1;
    int n;
    f1= fopen(nomFitxer,"ab");
    if( f1 == NULL ) {
        printf("Error en obrir el fitxer ");
        return -1;
    }
    entrarAnimal(&a1);
    n= fwrite(&a1,sizeof(Animal),1,f1);
    if(n==0) {
        printf("error d'escriptura");
        return -2;
    }
    fclose(f1);
    return 0;
}

int consulta(char nomFitxer[],bool esborrats){
    Animal a1;
    FILE *f1;
    int n;
    f1= fopen(nomFitxer,"rb");
    if( f1 == NULL ) {
        printf("Error en obrir el fitxer ");
        return -1;
    }
    system("clear");
    while(!feof(f1)){
        n = fread(&a1,sizeof(Animal), 1,f1);
        if(!feof(f1)){
            if(n==0) {
                printf("Error de lectura");
                return -3;
            }
            if(!esborrats){
                if(a1.marcaBorrat!='*')escriureAnimal(a1);
            }else{
                } if (a1.marcaBorrat=='*')escriureAnimal(a1);
        }
    }
    fclose(f1);
    printf("\nPerm una tecla per continuar..."); getchar();
    return 0;
}

void entrarAnimal(Animal *animal){
    char enPerill;
    printf("\nIntrodueix el nom: ");
    scanf("%15[^\n]",animal->nom);BB;
    printf("\nIntrodueix el tipus: ");
    scanf("%35[^\n]",animal->tipus);BB;
    do{
        printf("\nIntrodueix el sexe: ");
        scanf("%c",&animal->sexe);BB;
    }while(animal->sexe!='m' && animal->sexe!='f');
    printf("\nIntrodueix la edat: ");
    scanf("%d",&animal->edat);BB;
    printf("\nIntrodueix el pes: ");
    scanf("%lf",&animal->pes);BB;
    do{
        printf("\nEst� en perill d'extinci� (s/n)? ");
        scanf("%c",&enPerill);BB;
    }while(enPerill!='s' && enPerill!='n');
    //enPerillExtincio o �s 'n' o �s 's'
    if(enPerill=='s') animal->enPerillExtincio=true;
    else animal->enPerillExtincio=false;
     //animal->enPerillExtincio=(enPerill=='s')?true:false;
}
void escriureAnimal(Animal animal){
    printf("\nIntrodueix el nom: %s",animal.nom);
    printf("\nIntrodueix el tipus: %s",animal.tipus);
    printf("\nIntrodueix el sexe: %c",animal.sexe);
    printf("\nIntrodueix la edat: %d",animal.edat);
    printf("\nIntrodueix el pes: %lf",animal.pes);
    if(animal.enPerillExtincio==true) printf("\nEst� en perill\n") ;
    else printf("\nNo esta en perill d'extinci�\n") ;
}
int baixa(char nomfitxer[]){
    Animal a1;
    int n=0;
    FILE f;
    bool borrar=true;
    char nom[100];
    f=fopen(nomfitxer,"rb+");
    if(f=NULL){
    printf("Error en obrir el fitxer");
    return -1;
    }
    printf("\nIntrodueix el Nom? ");
    scanf("%9[^\n]",nom); while(getchar()!='\n');
    while(!feof(f)){
        n=fread(&a1,sizeof(Animal),1,f);
        if(!feof(f)){
            if(n==0){
                printf("Error de lectura");
                return -3;
            }
        escriureAnimal(a1);
        if(!borrar){
            break;
        }else{
            if(a1.marcaBorrat!='*'&&strcmp(a1.nom,nom)==0){
            if( fseek( f , -(long) sizeof(Animal) , SEEK_CUR ) ) {
                return -2; //error ... }
                a1.marcaBorrat='*';
                n=fwrite( &a1 , sizeof(Animal) , 1 , f);
            }
        break;
        if(n!=1){
        printf("\nBaixa: Error d'escriptura");
        return -2;
        }
        }
        }
        }
        }
}
int consultaresborrats(char nomfitxer[]){
    Animal a1;
    int n=0;
    FILE f;
    bool borrar=true;
    char nom[100];
    f=fopen(nomfitxer,"rb+");
    if(f=NULL){
    printf("Error en obrir el fitxer");
    return -1;
    }
    printf("\nIntrodueix el Nom? ");
    scanf("%9[^\n]",nom); while(getchar()!='\n');
    while(!feof(f)){
        n=fread(&a1,sizeof(Animal),1,f);
        if(!feof(f)){
            if(n==0){
                printf("Error de lectura");
                return -3;
            }
        escriureAnimal(a1);
        if(!borrar){
            break;
        }else{
            if(a1.marcaBorrat=='*'&&strcmp(a1.nom,nom)==0){
            if( fseek( f , -(long) sizeof(Animal) , SEEK_CUR ) ) {
                return -2; //error ... }
                escriureAnimal(a1);
            }
        break;
        if(n!=1){
        printf("\nBaixa: Error d'escriptura");
        return -2;
        }
        }
        }
        }
        }
}


int modificar(char nomfitxer[],Animal *animal){
 Animal a1;
    int n=0;
    int f=0;
    bool borrar=true;
    char nom[100];
    f=fopen(nomfitxer,"rb+");
    if(f==NULL){
    printf("Error en obrir el fitxer");
    return -1;
    }
   escriureAnimal(a1);
    while(!feof(f)){
        n=fread(&a1,sizeof(Animal),1,f);
        if(!feof(f)){
            if(n==0){
                printf("Error de lectura");
                return -3;
            }
        escriureAnimal(a1);
        if(!borrar){
            break;
        }else{
            if(a1.marcaBorrat!='*'&&strcmp(a1.nom,nom)==0){
            if( fseek( f , -(long) sizeof(Animal) , SEEK_CUR ) ) {
                return -2; //error ... }
                a1.marcaBorrat='*';
                n=fwrite( &a1 , sizeof(Animal) , 1 , f);
            }
        break;
        if(n!=1){
        printf("\nBaixa: Error d'escriptura");
        return -2;
        }
        }
        }
        }
        }


}
int numeroDeRegistres(nomFitxer[],Animal *animal){
FILE f;
Animal a1;
//situar-se al darrer registre
long nReg;
f=fopen(nomfitxer,"rb+");
    if(f==NULL){
    printf("Error en obrir el fitxer");
    return -1;
    }
nReg= ftell (f);//calcular el que ocupen els n-1 registres
nReg= nReg/sizeof(Animal); //calcular el n�mero de registres
if (a1==marcarborrats){
    nReg -1;
}
return nReg;
}
int accesDirecte(nomFitxer[],*nReg){
n= pos-1;
f=fopen(nomfitxer,"rb+");
    if(f==NULL){
    printf("Error en obrir el fitxer");
    return -1;
    }
    printf("A cual quieres acceder");
    scanf("%d",pos);
fseek(f, (*nReg) (n*sizeof(Animal)),SEEK_SET);
int pos,sentit;
(n*sizeof(Animal))
FILE *f;
fseek( f , (nReg) ( (sentit) * (n*sizeof(Animal)) ) , SEEK_SET);

}

/*bool seguir baixa(bool actualitza){
char opcio;
bool seguir=false;
do{
    if(actualitza){

    }
}
}
*/
void generarInforme(){
if((f=fopen(index.html,"a"))==NULL)
{printf("Error obrir fitxer");
control=-1;
}else{
do{cadena[0]='\0';
scanf("%80[^\n]",cadena);
while(getchar()!='\n');
strcat(cadena,"\n");
if(fputs(cadena,f)==EOF)
{printf("Errord'escriptura");
control=-1;
}
}
while(cadena[0]!='\n');
if(ferror(stdin))
{printf("error en llegir del teclat. Avortemelprograma.");
exit(-1);}if(fclose(f))
{printf("\nError tancarfitxer");
control=-1;
}
}
//LECTURA
if((f=fopen(nomFit,"r"))==NULL)
{printf("Error obrir fitxer");
control=-1;
}else{//entrada de text
printf("\n\nEl text llegit es:\n\t");
while(fgets(cadena,MAX_CAR,f))
printf("%s",cadena);
if(ferror(f))
{printf("error en llegir l'arxiu. Avortem el programa.");
exit(-1);
}
if(fclose(f))
{printf("\nError tancar fitxer");
control=-1;}}returncontrol;
return 0;
}
}
}
}
void esborrarFitcher(nomFitcher[]){
int unlink(nomFitxer);
}
int compactarFitxer(char nomFitxer[]){
    system("cls || clear");
    Animal animal;
    int n;
    FILE *origen, desti;
    origen = fopen(nomFitxer,"rb");
    if(origen == NULL) return -1;
    desti = fopen("tmp.dat","wb");
    if(desti == NULL) return -1;

    while(!feof(origen)){
        n = fread(&animal,sizeof(Animal),1,origen);
        if(!feof(origen)){
            if(n == 0) return -3;
            if(animal.marcaEsborrat != '') n = fwrite(&animal, sizeof(Animal),1,desti);
        }
    }
    fclose(origen);
    fclose(desti);
    esborrarFitxer(nomFitxer);
    if(rename("tmp.dat",NOM_FITXER) == -1) return -5;
    return 0;
}

